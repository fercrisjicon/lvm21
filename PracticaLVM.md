## Exercici Practic 1: utilitzar LVM
### Anunciado:
Crear tres unitats fisiques ‘imaginaries’ usant l’utilitat dd per generar espai de disc virtual. Assignar aquests fitxers a un dispositiu físic de loopback. És a dir, en lloc de crear tres particions de debò tipus /dev/sda2, /dev/sda3 i /dev/sda4 ens inventem les particions /dev/loop0, /dev/loop1 i /dev/loop2
```
Crear las imagenes
~# dd if=/dev/zero of=disk01.img bs=1k count=100k
102400+0 records in
102400+0 records out
104857600 bytes (105 MB, 100 MiB) copied, 0.1194 s, 878 MB/s
~# dd if=/dev/zero of=disk02.img bs=1k count=100K
102400+0 records in
102400+0 records out
104857600 bytes (105 MB, 100 MiB) copied, 0.128018 s, 819 MB/s
~# dd if=/dev/zero of=disk03.img bs=1k count=100K
102400+0 records in
102400+0 records out
104857600 bytes (105 MB, 100 MiB) copied, 0.117094 s, 895 MB/s
```
```
Asignar los loopback a las images
# losetup <LOOPBACK> <[DIR/]IMAGEN>

~# losetup /dev/loop0 disk01.img
~# losetup /dev/loop1 disk02.img
~# losetup /dev/loop2 disk03.img

~# losetup -a       # muestra todos los loopback
```
Ya disponemos de tres espacio de almacenamiento, ahora vamos a crear un volumen fisico a cada uno de ellos, es decir, que vamos a adaptarlos para que se usen como almacenaje LVM.
Este proceso se adecua el espacio para ser usado como espacio LVM nombrado como **Phisical Volumen**.
```
Crear un volumen fisico de una imagen/espacio
# pvcreate <LOOP_NAME>[...]

~# pvcreate /dev/loop0
  Physical volume "/dev/loop0" successfully created.
~# pv create /dev/loop1 /dev/loop2
  Physical volume "/dev/loop1" successfully created.
  Physical volume "/dev/loop2" successfully created.

***ANTES***
~# pvdisplay /dev/loop0     # muestra los atributos de un volumen fisico
  "/dev/loop0" is a new physical volume of "100.00 MiB"
  --- NEW Physical volume ---
  PV Name               /dev/loop0  # nombre PV
  VG Name               
  PV Size               100.00 MiB  # tamaño
  Allocatable           NO
  PE Size               0           # tamaño de la extension fisica
  Total PE              0           # espacio usado por los VG
  Free PE               0           # espacio libre
  Allocated PE          0
  PV UUID               yjFnmL-O2Wf-Gtiz-OJWU-M9UO-BOk5-gWLVd8
```
Los espacios de almacenamiento LVM (Phisical Volumen) se agrupan para crear unitags de almacenamiento (parecido a los discos fisicos para el sistema) nombrados **Volumen Groups** o grupo de volumen.
Ahora vamos a utilizar los 2 primeros para crear un nuevo Volumen Group llamado **diskedt**.
```
# vgcreate <NEW_VG_NAME> <LOOP_NAME>[...]

~# vgcreate diskedt /dev/loop0 /dev/loop1
  Volume group "diskedt" successfully created

~# vgdisplay diskedt    # muestra los atributos de un grupo de volumen
  --- Volume group ---
  VG Name               diskedt
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               **192.00 MiB**
  PE Size               **4.00 MiB**
  Total PE              48
  Alloc PE / Size       0 / 0   
  Free  PE / Size       **48 / 192.00 MiB**
  VG UUID               S70Vgk-NfNM-CfwM-e20e-tnlv-29N6-jArSKF
```
Loes espacios de loop0 y loop1 se han juntado para crear un nuevo dispositivo que el sistema considera un dispositivo fisico de 200M llamado **/dev/diskedt**(no es visible hasta que no lo particionemos). Ahora es de 192M debido a la necesidad de crear estructuras de datos para la gestion de LVM.
Vemos que cuando asignamos a loop0 y loop1 a un VG su descripion ha cambiado.
```
***DESPUES***
~# pvdisplay /dev/loop0
  --- Physical volume ---
  PV Name               /dev/loop0
  VG Name               diskedt
  PV Size               100.00 MiB / not usable 4.00 MiB
  Allocatable           yes 
  PE Size               4.00 MiB        # cambio
  Total PE              24              # cambio
  Free PE               24              # cambio
  Allocated PE          0
  PV UUID               yjFnmL-O2Wf-Gtiz-OJWU-M9UO-BOk5-gWLVd8
```
Al igual que un dispositivo logico puede hacer particiones, un dispositivo VG se puedes hacer particiones logicas llamadas **Logical Volume**.
En este ejemplo se hara dos particiones llamada ***sistema*** de 50M y la otra llamada ***dades*** de 150M. De dos PV de 100M se ha creado un VG de 200M que se ha divido en dos LV asimetricas.
```
# lvcreate -L <TAMAÑO> -n <NAME> <VOLUME_GROUP>
#   -L tamaño de la particion
#   -n nombre de la particion
#   -l tamaño de la particion(alternativa)

~# lvcreate -L 50M -n sistema /dev/diskedt
  Rounding up size to full physical extent 52.00 MiB
  Logical volume "sistema" created.
~# lvcreate -L 150M -n dades /dev/diskedt
  Rounding up size to full physical extent 152.00 MiB
  Volume group "diskedt" has insufficient free space (35 extents): 38 required.

Al parecer no hay suficiente espacio la segunda particion, entonce solo agarraremos el espacio libre/sobrante.
~# lvcreate -l100%FREE -n dades /dev/diskedt
  Logical volume "dades" created.

~# lvdisplay /dev/diskedt/sistema   # mostres los atributos de un volumen logico
  --- Logical volume ---
  LV Path                **/dev/diskedt/sistema**
  LV Name                sistema
  VG Name                diskedt
  LV UUID                M9Qk25-xzVC-IJVV-YdRu-FgYx-0tts-p8Be2t
  LV Write Access        read/write
  LV Creation host, time i10, 2022-02-09 09:18:36 +0100
  LV Status              available
  # open                 0
  LV Size                **52.00 MiB**
  Current LE             **13**
  **Segments**           **1**
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           254:0
~# lvdisplay /dev/diskedt/dades
  --- Logical volume ---
  LV Path                **/dev/diskedt/dades**
  LV Name                dades
  VG Name                diskedt
  LV UUID                IYRWPu-RCBn-w1MC-h0R0-ZuRy-iBvZ-LyM24s
  LV Write Access        read/write
  LV Creation host, time i10, 2022-02-09 09:19:44 +0100
  LV Status              available
  # open                 0
  LV Size                **140.00 MiB**
  Current LE             **35**
  **Segments**           **2**
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           254:1

~# tree /dev/disk
/dev/disk
├── by-id
│   ├── **dm-name-diskedt-dades -> ../../dm-1**
│   ├── **dm-name-diskedt-sistema -> ../../dm-0**
│   ├── dm-uuid-LVM-S70VgkNfNMCfwMe20etnlv29N6jArSKFIYRWPuRCBnw1MCh0R0ZuRyiBvZLyM24s -> ../../dm-1
│   ├── dm-uuid-LVM-S70VgkNfNMCfwMe20etnlv29N6jArSKFM9Qk25xzVCIJVVYdRuFgYx0ttsp8Be2t -> ../../dm-0
│   ├── lvm-pv-uuid-Eh2BHT-90P7-nmO0-oN4Q-afJe-Df8N-UAZwyi -> ../../loop1
│   ├── lvm-pv-uuid-phdepj-kpaC-FtBw-KrlH-j4if-6JPq-8YhMzo -> ../../loop2
│   ├── lvm-pv-uuid-yjFnmL-O2Wf-Gtiz-OJWU-M9UO-BOk5-gWLVd8 -> ../../loop0
│   ├── nvme-eui.00253857019e8fe7 -> ../../nvme0n1
│   ├── nvme-eui.00253857019e8fe7-part1 -> ../../nvme0n1p1
│   ├── nvme-eui.00253857019e8fe7-part5 -> ../../nvme0n1p5
│   ├── nvme-eui.00253857019e8fe7-part6 -> ../../nvme0n1p6
│   ├── nvme-eui.00253857019e8fe7-part7 -> ../../nvme0n1p7
│   ├── nvme-Samsung_SSD_970_EVO_Plus_500GB_S4EVNMFN728410J -> ../../nvme0n1
│   ├── nvme-Samsung_SSD_970_EVO_Plus_500GB_S4EVNMFN728410J-part1 -> ../../nvme0n1p1
│   ├── nvme-Samsung_SSD_970_EVO_Plus_500GB_S4EVNMFN728410J-part5 -> ../../nvme0n1p5
│   ├── nvme-Samsung_SSD_970_EVO_Plus_500GB_S4EVNMFN728410J-part6 -> ../../nvme0n1p6
│   ├── nvme-Samsung_SSD_970_EVO_Plus_500GB_S4EVNMFN728410J-part7 -> ../../nvme0n1p7
│   ├── usb-_USB_DISK_2.0_0700032A289C4C33-0:0 -> ../../sda
│   └── usb-_USB_DISK_2.0_0700032A289C4C33-0:0-part1 -> ../../sda1
├── by-label
│   ├── DEBIAN_MATI -> ../../nvme0n1p5
│   ├── TRABAJOS -> ../../sda1
│   └── \x2fDEBIAN_TARDE -> ../../nvme0n1p6
├── by-partuuid
│   ├── 7d750f95-01 -> ../../nvme0n1p1
│   ├── 7d750f95-05 -> ../../nvme0n1p5
│   ├── 7d750f95-06 -> ../../nvme0n1p6
│   ├── 7d750f95-07 -> ../../nvme0n1p7
│   └── a80e081b-01 -> ../../sda1
├── by-path
│   ├── pci-0000:00:14.0-usb-0:8:1.0-scsi-0:0:0:0 -> ../../sda
│   ├── pci-0000:00:14.0-usb-0:8:1.0-scsi-0:0:0:0-part1 -> ../../sda1
│   ├── pci-0000:0a:00.0-nvme-1 -> ../../nvme0n1
│   ├── pci-0000:0a:00.0-nvme-1-part1 -> ../../nvme0n1p1
│   ├── pci-0000:0a:00.0-nvme-1-part5 -> ../../nvme0n1p5
│   ├── pci-0000:0a:00.0-nvme-1-part6 -> ../../nvme0n1p6
│   └── pci-0000:0a:00.0-nvme-1-part7 -> ../../nvme0n1p7
└── by-uuid
    ├── 74e60d8f-46c5-46a0-8e35-1d6d9a793388 -> ../../nvme0n1p5
    ├── 76e5d083-a0e9-44fa-a21b-dbe4b299d0d0 -> ../../nvme0n1p7
    ├── B84B-A630 -> ../../sda1
    └── e7333161-fef1-4e78-bcb6-91dd4a126480 -> ../../nvme0n1p6

5 directories, 38 files

~# ls -l /dev/diskedt/
total 0
lrwxrwxrwx 1 root root 7 Feb  9 09:19 dades -> ../dm-1
lrwxrwxrwx 1 root root 7 Feb  9 09:18 sistema -> ../dm-0
```
### Resumir luego
Observar la cantidad de PE que utiliza cada Logical Volume, sistema utiliza 13 de 4MB que proporcionan 52MB de almacenamiento, todos ellos en un mismo segmento (Phisical Volumen). En cambio datos utiliza 35 unidades de asignación PE de 4MB proporcionando 140MB de almacenamiento y ocupa dos segmentos, es decir, utiliza ambos Phisical Volumes.
Ahora ya está todo listo para poder formatear estos volúmenes lógicos y poderlos integrar en el sistema de archivos montándolos donde se crea oportuno. Una vez montados se utilizan como cualquier otro elemento de almacenamiento.
```
~# mkfs -t ext4 /dev/diskedt/dades
mke2fs 1.46.2 (28-Feb-2021)
Discarding device blocks: done                            
Creating filesystem with 143360 1k blocks and 35856 inodes
Filesystem UUID: 85ddf23d-eebf-4954-983d-0f5b240f8184
Superblock backups stored on blocks: 
	8193, 24577, 40961, 57345, 73729

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (4096 blocks): done
Writing superblocks and filesystem accounting information: done

~# mkfs -t ext4 /dev/diskedt/sistema
mke2fs 1.46.2 (28-Feb-2021)
Discarding device blocks: done                            
Creating filesystem with 53248 1k blocks and 13328 inodes
Filesystem UUID: 0ef55b62-f9d4-4425-a657-4d53c1091c4d
Superblock backups stored on blocks: 
	8193, 24577, 40961

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (4096 blocks): done
Writing superblocks and filesystem accounting information: done


Crear puntos de montage y montarlos
~# mkdir /mnt/dades
~# mkdir /mnt/sistema
~# mount /dev/diskedt/dades /mnt/dades
~# mount /dev/diskedt/sistema /mnt/sistema

Copiar datos dentro de cada directori
~# cp -R /boot/* /mnt/dades/
~# cp /boot/* /mnt/sistema/

~# df -h
Filesystem                                                            Size  Used Avail Use% Mounted on
...
/dev/mapper/diskedt-dades                                             131M   65M   56M  54% /mnt/dades
/dev/mapper/diskedt-sistema                                            46M   45M     0 100% /mnt/sistema

~# blkid
...
/dev/mapper/diskedt-sistema: UUID="0ef55b62-f9d4-4425-a657-4d53c1091c4d" BLOCK_SIZE="1024" TYPE="ext4"
/dev/mapper/diskedt-dades: UUID="85ddf23d-eebf-4954-983d-0f5b240f8184" BLOCK_SIZE="1024" TYPE="ext4"

```
# Acabar, en casa, la ultima practica de LVM: Montaje en caliente.
